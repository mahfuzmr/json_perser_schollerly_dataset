import json


# n = 1
#
# storedfile = "in/semantic_web/full_Json_AIDA_SW.json"
#
# for i in range(1, 9):
#     data = []
#     in_json_file = "in/semantic_web/SW/semantic_web_" + str(n) + ".json"
#     with open(in_json_file) as f:
#         data = json.load(f)
#         print(i, len(data))
#         if len(data) > 0:
#             with open(storedfile, 'a+',newline="\n") as json_file:
#                 # json.dump(data, json_file)
#                 for item in data:
#                     json_file.write(json.dumps(item, ensure_ascii=False,separators=(",",":"))+"\n")
#             json_file.close()
#     f.close()
# print("Full AIDA writing complete for semantic web dataset")
#
#
#
# n = 1
#
# storedfile = "in/neural_networks/full_Json_AIDA_NN.json"
#
# for i in range(1, 143):
#     data = []
#     in_json_file = "in/neural_networks/NN/neural_networks_" + str(n) + ".json"
#     with open(in_json_file) as f:
#         data = json.load(f)
#         print(i, len(data))
#         if len(data) > 0:
#             with open(storedfile, 'a+',newline="\n") as json_file:
#                 # json.dump(data, json_file)
#                 for item in data:
#                     json_file.write(json.dumps(item, ensure_ascii=False,separators=(",",":"))+"\n")
#             json_file.close()
#     f.close()
# print("Full AIDA writing complete neural network")


storedfile = "in/author.json"
in_json_file = "in/Francesco_Osborne.json"
with open(in_json_file) as f:
    data = json.load(f)
    if len(data) > 0:
        with open(storedfile, 'a+',newline="\n") as json_file:
            # json.dump(data, json_file)
            for item in data:
                json_file.write(json.dumps(item, ensure_ascii=False,separators=(",",":"))+"\n")
        json_file.close()
f.close()
print("Full AIDA writing complete for semantic web dataset")