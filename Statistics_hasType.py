import json
import math
import pickle
import os
import io
import termcolor
import random
from datetime import datetime
import numpy as np
import pandas as pd
#
# in_json_file = "in/full_graph.json"
# total_lines = str(sum(1 for line in open(in_json_file)))
# education=0
# Goverment=0
# company=0
# other=0
# with open(in_json_file) as file:
#     for (index, line) in enumerate(file):
#         if index < 6000:
#             elem = json.loads(line)
#             filter_condition = False
#             if "types" in list(elem.keys()):
#                 typeLists = list(elem["types"].keys())
#                 if "Company" in typeLists :
#                     company +=1
#                 elif "Education" in typeLists :
#                     education+=1
#                 elif "Government" in typeLists:
#                     Goverment +=1
#                 else:
#                     other +=1
#
#     print("Education: "+str(education)+"\n"+"Government: "+str(Goverment)+"\n"+"Company: "+str(company)+"\n"+"other:"+str(other))
#     print("total: "+str(education+Goverment+company+other))

out_test = "out/Education papers/test.txt"
columns = ["subject", "predicate", "object"]
all_triples_test = pd.read_table(out_test, delimiter='\t', names=columns, header=None)
all_triples_test = pd.DataFrame(all_triples_test)

out_valid= "out/Education papers/valid.txt"
all_triples_valid = pd.read_table(out_valid, delimiter='\t', names=columns, header=None)
all_triples_valid = pd.DataFrame(all_triples_valid)

out_train= "out/Education papers/train.txt"
all_triples_train = pd.read_table(out_train, delimiter='\t', names=columns, header=None)
all_triples_train = pd.DataFrame(all_triples_train)

result = all_triples_test.merge(all_triples_train)

print(result.predicate.to_string(index=False))

print("finished for test set")

result = all_triples_valid.merge(all_triples_train)

print(result.predicate.to_string(index=False))


print("Finished")






