
import io
import numpy as np
import pandas as pd


out_test_txt="out/Education papers/test.txt"
columns=["subject", "predicate", "object"]
all_triples = pd.read_table(out_test_txt,delimiter='\t',names=columns, header=None)
all_triples = pd.DataFrame(all_triples)
data_hasType = all_triples[all_triples["predicate"] == "hasTypes"]
unique_hasTypes=data_hasType["object"].unique().tolist()

print("Writing for test_hasTypes\n")
out_test_hasType="out/Education papers/test_hasTypes.txt"
file_hasType = open(out_test_hasType,"a+")
np.savetxt(file_hasType, data_hasType.values, fmt='%s', delimiter='\t', newline='\n', header='',
        footer='', comments='', encoding=None)
file_hasType.close()
#
# print("Process running for entity and relation creation\n")
# out_relations_dict = "out/Education papers/relations_hasType.dict"
# out_entities_dict = "out/Education papers/entities_hasType.dict"
#
# out_data_nt = "out/Education papers/test_hasTypes.txt"
# # out_data_nt="out/trial/data.nt"
# print("Data loading...\n")
# columns = ["subject", "predicate", "object"]
# all_triples = pd.read_table(out_data_nt, delimiter='\t', names=columns, header=None)
# all_triples = pd.DataFrame(all_triples)
# print("Data loading successful\n")
# print("Creating entity..")
# unique_entity = all_triples["subject"].astype(str).unique().tolist()
# unique_entity += all_triples["object"].astype(str).unique().tolist()
#
# entity_file = io.open(out_entities_dict, "a+", encoding="utf-8")
# index = 1
# for idx, elem in enumerate(unique_entity):
#     entity_file.write(str(index) + "\t" + str(elem).replace(" ", "_") + "\n")
#     index += 1
# entity_file.close()
#
# print("Creating relation")
# unique_relations = all_triples["predicate"].astype(str).unique().tolist()
# entity_file = io.open(out_relations_dict, "a+", encoding="utf-8")
# index = 1
# for idx, elem in enumerate(unique_relations):
#     entity_file.write(str(index) + "\t" + str(elem).replace(" ", "_") + "\n")
#     index += 1
# entity_file.close()



print("Writing complete")
