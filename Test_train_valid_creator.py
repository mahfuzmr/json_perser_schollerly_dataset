import random
import csv
import io
import numpy as np
import pandas as pd

out_train_file = "out/Education papers/train.txt"
out_test_file = "out/Education papers/test.txt"
out_val_file = "out/Education papers/valid.txt"

out_data_nt="out/Education papers/data.nt"
out_relation="out/Education papers/relations.dict"

# out_data_nt="out/trial/data.nt"
# out_relation="out/trial/relations.dict"

# data_file= open(out_data_nt)
relations= open(out_relation)
rel_reader =csv.reader(relations, delimiter='\t')
relations=[]
for rel in rel_reader:
   relations.append(rel[1])
columns=["subject", "predicate", "object"]
all_triples = pd.read_table(out_data_nt,delimiter='\t',names=columns, header=None)
# all_triples = np.genfromtxt(fname=out_data_nt, dtype=str)
all_triples = pd.DataFrame(all_triples)
print("Loading successful")

for rel in relations:
    data_per_rel = all_triples[all_triples["predicate"]==rel]
    data_per_rel= data_per_rel.sample(frac=1)

    print("Writing for relation "+str(rel))
    #80 %training
    print("\nWriting for train data for relation : " + str(rel))
    size = len(data_per_rel)
    train_data =  data_per_rel[0 : int(size * .80)]
    file_train = open(out_train_file,"a+")
    np.savetxt(file_train, train_data.values, fmt='%s', delimiter='\t', newline='\n', header='',
            footer='', comments='', encoding=None)
    file_train.close()
    #10 %test
    print("\nWriting for test data for relation : " + str(rel))
    test_data = data_per_rel[int(size * 0.80) :int(0.90 * size)]
    if len(test_data)>0:
        file_test = open(out_test_file, "a+")
        np.savetxt(file_test, test_data.values, fmt='%s', delimiter='\t', newline='\n', header='',
                   footer='', comments='', encoding=None)
        file_test.close()
    # #10 %valid
    print("\nWriting for valid data for relation : " + str(rel))
    valid_data = data_per_rel[int(size * .90) :size]
    if len(valid_data) > 0:
        file_valid = open(out_val_file, "a+")
        np.savetxt(file_valid, valid_data.values, fmt='%s', delimiter='\t', newline='\n', header='',
                   footer='', comments='', encoding=None)
        file_valid.close()
print("Process executed successfully")











