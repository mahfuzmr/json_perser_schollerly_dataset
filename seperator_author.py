import json
import math
import pickle
import os
import io
import termcolor
import random
from datetime import datetime
import numpy as np
import pandas as pd


def refresh_entity_table(line: str, tables: dict, entities: list, relations: list) -> tuple:
    if tables is None:
        # first create empty dict table
        tables = dict()
        tables["__relation__"] = dict()
        entities = []
        relations = []

    elem = json.loads(line)
    filter_condition = True

    if filter_condition:
        authorId = ''
        for key, value in elem.items():
            # first check if the key exists and add it if it does not

            if key not in tables.keys():
                if key == "mag_id" and value.__class__.__name__ not in ("dict", "list"):
                    new_rel = "authorId"
                    authorId=value
                else:
                    new_rel = "has" + key.capitalize()
                if new_rel not in relations and value.__class__.__name__ not in ("dict", "list"):
                    relations.append(new_rel)
                    tables[new_rel] = dict()
                    tables[new_rel] = value
                if value.__class__.__name__ in ("dict", "list"):
                    if key== "uniqueEntities":
                        for item in value:
                            subvalue=value[item]
                            if subvalue.__class__.__name__ in ("dict") and item=="organizations":
                                new_rel = "has" + item.capitalize()
                                tables[new_rel] = dict()
                                count=0

                                for subitem in subvalue:
                                    # tables[new_rel][count]=subitem
                                    # count +=1
                                    tables[new_rel][count] = dict()
                                    subsubvalue=subvalue[subitem]
                                    tables[new_rel][count]["orgaName"]=subsubvalue["name"].replace(" ", "_").strip("_")
                                    tables[new_rel][count]["orgaPublication"] = subsubvalue["num_publications"]
                                    tables[new_rel][count]["orgaCitations"] = subsubvalue["num_citations"]
                                    count += 1

                            if subvalue.__class__.__name__ in ("dict") and item == "topics":
                                new_rel = "has" + item.capitalize()
                                tables[new_rel] = dict()
                                count = 0

                                for subitem in subvalue:
                                    # tables[new_rel][count] = subitem
                                    tables[new_rel][count] = dict()
                                    subsubvalue = subvalue[subitem]
                                    tables[new_rel][count]["topicName"] = subsubvalue["name"].replace(" ", "_").strip("_")
                                    tables[new_rel][count]["topicPublication"] = subsubvalue["num_publications"]
                                    tables[new_rel][count]["topicCitations"] = subsubvalue["num_citations"]
                                    count += 1
                            if subvalue.__class__.__name__ in ("dict") and item == "authors":
                                new_rel = "hasCo" + item.capitalize()
                                tables[new_rel] = dict()
                                count = 0

                                for subitem in subvalue:
                                    if authorId !=subitem:
                                        # tables[new_rel][count]["name"] = subitem
                                        # count += 1
                                        tables[new_rel][count] = dict()
                                        subsubvalue = subvalue[subitem]
                                        tables[new_rel][count]["coAuthorName"] = subsubvalue["name"].replace(" ", "_").strip("_")
                                        tables[new_rel][count]["coAuthorId"] = subsubvalue["mag_id"]
                                        tables[new_rel][count]["coAuthorPublications"] = subsubvalue["num_publications"]
                                        tables[new_rel][count]["coAuthorCitations"] = subsubvalue["num_citations"]
                                        count += 1
                            if subvalue.__class__.__name__ in ("dict") and item == "countries":
                                new_rel = "has" + item.capitalize()
                                tables[new_rel] = dict()
                                count = 0

                                for subitem in subvalue:
                                    # tables[new_rel][count] = subitem
                                    tables[new_rel][count] = dict()
                                    subsubvalue = subvalue[subitem]
                                    tables[new_rel][count]["countryName"] = subsubvalue["name"].replace(" ", "_").strip("_")
                                    tables[new_rel][count]["countryPublication"] = subsubvalue["num_publications"]
                                    tables[new_rel][count]["countryCitations"] = subsubvalue["num_citations"]
                                    count += 1
                            if subvalue.__class__.__name__ in ("dict") and item == "conferences":
                                new_rel = "has" + item.capitalize()
                                tables[new_rel] = dict()
                                count = 0

                                for subitem in subvalue:
                                    # tables[new_rel][count] = subitem
                                    tables[new_rel][count] = dict()
                                    subsubvalue = subvalue[subitem]
                                    tables[new_rel][count]["confName"] = subsubvalue["name"].replace(" ","_").strip("_")
                                    tables[new_rel][count]["confPublication"] = subsubvalue["num_publications"]
                                    tables[new_rel][count]["confCitations"] = subsubvalue["num_citations"]
                                    count += 1
                            if subvalue.__class__.__name__ in ("dict") and item == "journals":
                                new_rel = "has" + item.capitalize()
                                tables[new_rel] = dict()
                                count = 0

                                for subitem in subvalue:
                                    # tables[new_rel][count] = subitem
                                    tables[new_rel][count] = dict()
                                    subsubvalue = subvalue[subitem]
                                    tables[new_rel][count]["jourName"] = subsubvalue["name"].replace(" ","_").strip("_")
                                    tables[new_rel][count]["jourPublication"] = subsubvalue["num_publications"]
                                    tables[new_rel][count]["jourCitations"] = subsubvalue["num_citations"]
                                    count += 1



            else:
                raise Exception("Unresolved branch!")

    return tables, entities, relations


def get_entity_tables(load_indicator: bool, in_json_file: str, out_bin_file: str, out_ent_file: str, out_rel_file: str,
                      verbose: bool = False) -> dict:
    all_entities = []
    all_relations = []
    if load_indicator is False:

        if verbose:
            print(termcolor.colored("Scanning...", color="magenta"))

        with open(in_json_file) as file:
            total_lines = str(sum(1 for line in open(in_json_file)))
            for (index, line) in enumerate(file):
                if index >= 0 :
                    print("total " + str(index) + " outof " + total_lines)
                    tables = None
                    entities = []
                    relations = []


                    (tables, entities, relations) = refresh_entity_table(line=line, tables=tables, entities=entities,
                                                                    relations=relations)


        print(termcolor.colored("Data.nt file created successfully ", color="green"), end="")

        print(termcolor.colored("Process running for entity and relation creation\n", color="red"), end="")
        out_relations_dict = "out/Education papers/relations.dict"
        out_entities_dict = "out/Education papers/entities.dict"

        out_data_nt = "out/Education papers/data.nt"
        # out_data_nt="out/trial/data.nt"
        print(termcolor.colored("Data loading...\n", color="cyan"), end="")
        columns = ["subject", "predicate", "object"]
        all_triples = pd.read_table(out_data_nt, delimiter='\t', names=columns, header=None)
        all_triples = pd.DataFrame(all_triples)
        print(termcolor.colored("Data loading successful\n", color="red"), end="")
        print("Creating entity..")
        unique_entity = all_triples["subject"].astype(str).unique().tolist()
        unique_entity += all_triples["object"].astype(str).unique().tolist()
        unique_entity = list(set(unique_entity))

        entity_file = io.open(out_entities_dict, "w+", encoding="utf-8")
        index = 0
        for idx, elem in enumerate(unique_entity):
            entity_file.write(str(index) + "\t" + str(elem).replace(" ", "_") + "\n")
            index += 1
        entity_file.close()

        print("Creating relation")
        unique_relations = all_triples["predicate"].astype(str).unique().tolist()
        entity_file = io.open(out_relations_dict, "w+", encoding="utf-8")
        index = 0
        for idx, elem in enumerate(unique_relations):
            entity_file.write(str(index) + "\t" + str(elem).replace(" ", "_") + "\n")
            index += 1
        entity_file.close()


def main():
    in_json_file = "in/author.json"
    tables = get_entity_tables(load_indicator=False, in_json_file=in_json_file,
                               out_bin_file="out/Authors/tables.bin",
                               out_ent_file="out/Authors/entities.dict",
                               out_rel_file="out/Authors/relations.dict", verbose=True)
    print("For the creation of data entity and relation has been complete...\ closing down....")
    pass


if __name__ == "__main__":
    main()
    pass
else:
    pass
