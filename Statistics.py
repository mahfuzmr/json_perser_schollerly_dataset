import random
import csv
import io
import numpy as np
import pandas as pd

out_test_txt = "out/Education papers/data.nt"
out_frequency = "out/Education papers/freq_relation.txt"
out_negetive_sample = "out/Education papers/test_neg.txt"

columns = ["subject", "predicate", "object"]
all_triples = pd.read_table(out_test_txt, delimiter='\t', names=columns, header=None)
all_triples = pd.DataFrame(all_triples)
print(all_triples.shape)
freq_relation = all_triples["predicate"].value_counts().to_dict()
print("Writing all the relation frequency\n")
file_freq = open(out_frequency, "w+")
file_freq.write("Frequency for all the relation in the test.txt data set \n")
for elem in freq_relation:
    file_freq.write(str(elem) + ": " + str(freq_relation[elem]) + "\n")
file_freq.close()

# -----------------------------------data that only has hasTypes-------------------------------------------------
data_hasType = all_triples[all_triples["predicate"] == "hasType"]
unique_hasTypes_list = data_hasType["object"].unique().tolist()
negeted_triple = pd.DataFrame(columns = columns)
for data in unique_hasTypes_list:
    list_except_date = [x for x in unique_hasTypes_list if x != data]
    triple_contain_data = data_hasType[data_hasType["object"] == data]
    triple_contain_data["object"] = triple_contain_data["object"].apply(
        lambda v: random.choice(list_except_date))
    negeted_triple=negeted_triple.append(triple_contain_data)

# print("Generating negative triple for hasType")
# file_ns = open(out_negetive_sample, "w+")
# np.savetxt(file_ns, negeted_triple.values, fmt='%s', delimiter='\t', newline='\n', header='',
#            footer='', comments='', encoding=None)
# file_ns.close()

out_data_nt = "out/Education papers/SW/data.nt"
columns = ["subject", "predicate", "object"]
data_triples = pd.read_table(out_data_nt, delimiter='\t', names=columns, header=None)
data_triples = pd.DataFrame(data_triples)
# hasTypes
data_hasTypes = data_triples[data_triples["predicate"] == "hasType"]
unique_type = data_hasTypes["object"].astype(str).unique().tolist()



data_hasTopics = data_triples[data_triples["predicate"] == "hasTopics"]
unique_hasTopics = data_hasTopics["object"].astype(str).unique().tolist()


print("Generating Unique value for hasType")
out_unique_hastype="out/Education papers/uniqetype.txt"
unique_freq = open(out_unique_hastype, "w+")
for re_type in unique_type:
    unique_freq.write(str(re_type) + "\n")

for re_Topics in unique_hasTopics:
    unique_freq.write(str(re_Topics) + "\n")

unique_freq.close()
print("Finished")
