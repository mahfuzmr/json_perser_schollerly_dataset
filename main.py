import json
import math
import pickle
import os
import io
import termcolor
import random


def refresh_entity_table(line: str, tables: dict, entities: list, relations: list) -> tuple:

    if tables is None:
        # first create empty dict table
        tables = dict()
        tables["__relation__"] = dict()
        entities = []
        relations = []

    elem = json.loads(line)

    filter_condition = "types" in list(elem.keys()) and "Healthcare" in list(elem["types"].keys())
    # filter_condition = True

    if filter_condition:

        for key, value in elem.items():

            # first check if the key exists and add it if it does not
            if key not in tables.keys():
                new_key = "rel" + "_" + str(len(tables["__relation__"].keys()))
                tables["__relation__"][key] = new_key
                relations.append(new_key)
                tables[key] = dict()

            # insert regular element
            if value.__class__.__name__ not in ("dict", "list"):
                if value not in tables[key].keys():
                    new_key = key + "_" + str(len(tables[key].keys()))
                    tables[key][value] = new_key
                    entities.append(new_key)

            # if element is a list, process each one of its subelements accordingly
            elif value.__class__.__name__ == "list":
                for subvalue in value:

                    # if list is made up of dicts, add the keys as relations and fields as values
                    if subvalue.__class__.__name__ == "dict":
                        for (subsubkey, subsubvalue) in subvalue.items():
                            if key == "authors" and subvalue["id"] not in tables[key].keys():
                                new_key = key + "_" + str(len(tables[key].keys()))
                                tables[key][subvalue["id"]] = new_key
                                entities.append(new_key)
                            if subsubkey not in tables.keys():
                                new_key = "rel" + "_" + str(len(tables["__relation__"].keys()))
                                tables["__relation__"][subsubkey] = new_key
                                relations.append(new_key)
                                tables[subsubkey] = dict()
                            if subsubvalue not in tables[subsubkey].keys():
                                new_key = subsubkey + "_" + str(len(tables[subsubkey].keys()))
                                tables[subsubkey][subsubvalue] = new_key
                                entities.append(new_key)

                    # if list is made up of dicts, add the keys as relations and fields as values
                    elif subvalue.__class__.__name__ == "list":
                        for subelem in subvalue:
                            if subelem not in tables[key].keys():
                                new_key = key + "_" + str(len(tables[key].keys()))
                                tables[key][subvalue] = new_key
                                entities.append(new_key)

                    # if list is made up of regular elements, add the regular elements
                    else:
                        if subvalue not in tables[key].keys():
                            new_key = key + "_" + str(len(tables[key].keys()))
                            tables[key][subvalue] = new_key
                            entities.append(new_key)

            # if element is a dict, process each one of its subelements accordingly
            elif value.__class__.__name__ == "dict":
                for (subkey, subvalue) in value.items():
                    subvalue = round(subvalue, 2)
                    if subkey not in tables[key].keys():
                        new_key = key + "_" + str(len(tables[key].keys()))
                        tables[key][subkey] = new_key
                        entities.append(new_key)
                    # if subvalue not in tables["intensity"].keys():
                    #     tables["intensity"][subvalue] = None

            # all other cases
            else:
                raise Exception("Unresolved branch!")

    return tables, entities, relations


def get_entity_tables(load_indicator: bool, in_json_file: str, out_bin_file: str, out_ent_file: str, out_rel_file: str, verbose: bool = False) -> dict:

    tables = None
    entities = []
    relations = []

    if load_indicator is False:

        if verbose:
            print(termcolor.colored("Scanning...", color="magenta"))

        with open(in_json_file) as file:
            total_lines = sum(1 for line in open(in_json_file))
            for (index, line) in enumerate(file):
                (tables, entities, relations) = refresh_entity_table(line=line, tables=tables, entities=entities, relations=relations)
                if verbose and ((index % 1000 == 0) or (index == total_lines - 1)):
                    print(termcolor.colored("Finished scanning ", color="magenta"), end="")
                    print("{:05.2f}".format(round(100*index/(total_lines - 1), 2)), end="%")
                    print(termcolor.colored(" of lines...", color="magenta"))

        if len(entities) != len(list(set(entities))):
            raise Exception("Duplicate entities!")

        # generate abbrebiations for previously created/loaded entitiy table

        if verbose:
            print(termcolor.colored("Finished scanning", color="magenta"))

        if verbose:
            print(termcolor.colored("Saving entity and relation files... ", color="magenta"))

        file_ent = io.open(out_ent_file, "a+", encoding="ascii")
        file_ent.truncate(0)
        for (index, elem) in enumerate(entities):
            file_ent.write(str(index) + "\t" + elem + "\n")
        file_ent.close()

        if verbose:
            print(termcolor.colored("Entity file written to ", color="magenta"), end="")
            print(out_ent_file)

        file_rel = io.open(out_rel_file, "a+", encoding="ascii")
        file_rel.truncate(0)
        for (index, elem) in enumerate(relations):
            file_rel.write(str(index) + "\t" + elem + "\n")
        file_rel.close()

        if verbose:
            print(termcolor.colored("Relation file written to ", color="magenta"), end="")
            print(out_rel_file)

    if load_indicator is False and out_bin_file is not None:
        # save fully created bin file
        with open(out_bin_file, "wb") as file:
            if verbose:
                print(termcolor.colored("Saving tables... ", color="magenta"))
            pickle.dump(tables, file, protocol=pickle.HIGHEST_PROTOCOL)
            if verbose:
                print(termcolor.colored("Tables saved to ", color="magenta"), end="")
                print(out_bin_file)
    if load_indicator is True and out_bin_file is not None:
        # load fully created bin file
        with open(out_bin_file, "rb") as file:
            if verbose:
                print(termcolor.colored("Loading tables... ", color="magenta"), end="")
            tables = pickle.load(file)
            if verbose:
                print(termcolor.colored("Tables loaded from ", color="magenta"), end="")
                print(out_bin_file)

    return tables


def encode_graph(in_json_file: str, tables: dict, out_raw_file: str, out_enc_file: str, verbose: bool = False):

    if verbose:
        print(termcolor.colored("Encoding...", color="magenta"))

    file_raw = io.open(out_raw_file, "a+", encoding="utf-8")
    file_raw.truncate(0)
    file_encoded = io.open(out_enc_file, "a+", encoding="ascii")
    file_encoded.truncate(0)

    with open(in_json_file) as file:
        total_lines = sum(1 for line in open(in_json_file))
        for (index, line) in enumerate(file):

            elem = json.loads(line)

            filter_condition = "types" in list(elem.keys()) and "Healthcare" in list(elem["types"].keys())
            # filter_condiiton = True

            if filter_condition:

                for key, value in elem.items():
                    if key != "id":

                        # process regular element
                        if value.__class__.__name__ not in ("dict", "list"):
                            new_entry_raw = str(elem["id"]) + "\t" + str(key) + "\t" + str(value) + "\n"
                            file_raw.write(new_entry_raw)
                            new_entry_encoded = tables["id"][elem["id"]] + "\t" + tables["__relation__"][key] + "\t" + tables[key][value] + "\n"
                            file_encoded.write(new_entry_encoded)

                        # if element is a list, process each one of its subelements accordingly
                        elif value.__class__.__name__ == "list":
                            for subvalue in value:

                                # if list is made up of dicts, add the keys as relations and fields as values
                                if subvalue.__class__.__name__ == "dict":

                                    # add ID - relation - ID link  entry
                                    new_entry_raw = str(elem["id"]) + "\t" + str(key) + "\t" + str(subvalue["id"]) + "\n"
                                    file_raw.write(new_entry_raw)
                                    new_entry_encoded = tables["id"][elem["id"]] + "\t" + tables["__relation__"][key] + "\t" + tables[key][subvalue["id"]] + "\n"
                                    file_encoded.write(new_entry_encoded)

                                    # add subentries
                                    for (subsubkey, subsubvalue) in subvalue.items():
                                        if subsubkey != "id":
                                            new_entry_raw = str(subvalue["id"]) + "\t" + str(subsubkey) + "\t" + str(subsubvalue) + "\n"
                                            file_raw.write(new_entry_raw)
                                            new_entry_encoded = tables["id"][subvalue["id"]] + "\t" + tables["__relation__"][subsubkey] + "\t" + tables[subsubkey][subsubvalue] + "\n"
                                            file_encoded.write(new_entry_encoded)

                                # if list is made up of regular elements, add the regular elements
                                else:
                                    new_entry_raw = str(elem["id"]) + "\t" + str(key) + "\t" + str(subvalue) + "\n"
                                    file_raw.write(new_entry_raw)
                                    new_entry_encoded = tables["id"][elem["id"]] + "\t" + tables["__relation__"][key] + "\t" + tables[key][subvalue] + "\n"
                                    file_encoded.write(new_entry_encoded)

                        # if element is a dict, process each one of its subelements accordingly
                        elif value.__class__.__name__ == "dict":
                            for (subkey, subvalue) in value.items():
                                new_entry_raw = str(elem["id"]) + "\t" + str(key) + "\t" + str(subkey) + "\n"
                                file_raw.write(new_entry_raw)
                                new_entry_encoded = tables["id"][elem["id"]] + "\t" + tables["__relation__"][key] + "\t" + tables[key][subkey] + "\n"
                                file_encoded.write(new_entry_encoded)

                                pass
                        # all other cases
                        else:
                            raise Exception("Unresolved branch!")

            if verbose and ((index % 1000 == 0) or (index == total_lines - 1)):
                print(termcolor.colored("Finished encoding ", color="magenta"), end="")
                print("{:05.2f}".format(round(100*index/(total_lines - 1), 2)), end="%")
                print(termcolor.colored(" of lines...", color="magenta"))

        if verbose:
            print(termcolor.colored("Finished encoding...", color="magenta"))

    file_raw.close()
    file_encoded.close()


def split_train_test_val(out_enc_file: str, out_train_file: str, out_test_file:str, out_val_file:str, total_ratio: float, train_ratio: float, test_ratio: float, val_ratio: float):

    if (train_ratio + test_ratio + val_ratio) != 1.00:
        raise Exception("Total ratio is not equal to one!")

    file_enc = io.open(out_enc_file, "r", encoding="ascii")
    file_train = io.open(out_train_file, "w", encoding="ascii")
    file_train.truncate(0)
    file_test = io.open(out_test_file, "w", encoding="ascii")
    file_test.truncate(0)
    file_val = io.open(out_val_file, "w", encoding="ascii")
    file_val.truncate(0)
    for (index, row) in enumerate(file_enc):
        selector = random.uniform(0, 1)
        if selector <= total_ratio:
            selector = random.uniform(0, 1)
            if 0 <= selector < train_ratio:
                file_train.write(row)
            elif train_ratio <= selector < train_ratio + test_ratio:
                file_test.write(row)
            elif train_ratio + test_ratio <= selector <= 1.0:
                file_val.write(row)

    file_train.close()
    file_test.close()
    file_val.close()
    file_enc.close()


def main():

    in_json_file = "in/full_graph.json"

    tables = get_entity_tables(load_indicator=False, in_json_file=in_json_file, out_bin_file="out/tables.bin", out_ent_file="out/entities.dict", out_rel_file="out/relations.dict", verbose=True)
    encode_graph(in_json_file=in_json_file, tables=tables, out_raw_file="out/data_raw.txt", out_enc_file="out/data.txt", verbose=True)
    split_train_test_val(out_enc_file="out/data.txt",
                         out_train_file="out/train.txt",
                         out_test_file="out/test.txt",
                         out_val_file="out/valid.txt",
                         total_ratio=1.0,
                         train_ratio=0.6,
                         test_ratio=0.2,
                         val_ratio=0.2)


    pass


if __name__ == "__main__":
    main()
    pass
else:
    pass
