import json
import math
import pickle
import os
import io
import termcolor
import random
from datetime import datetime
import numpy as np
import pandas as pd


def refresh_entity_table(line: str, tables: dict, entities: list, relations: list) -> tuple:
    if tables is None:
        # first create empty dict table
        tables = dict()
        tables["__relation__"] = dict()
        entities = []
        relations = []

    elem = json.loads(line)
    filter_condition = False
    if "types" in list(elem.keys()):
        # typeLists = list(elem["types"].keys())
        # filter_condition = "Company" in typeLists or "Education" in typeLists or "Government" in typeLists
        filter_condition = True

    if filter_condition:
        for key, value in elem.items():
            # first check if the key exists and add it if it does not
            if key not in tables.keys():
                if key == "id" and value.__class__.__name__ == "int":
                    new_rel = "Paper" + "_" + key.capitalize()
                else:
                    new_rel = "has" + key.capitalize()
                tables["__relation__"][key] = new_rel
                if new_rel not in relations:
                    relations.append(new_rel)
                    tables[key] = dict()

            if value.__class__.__name__ not in ("dict", "list"):
                new_entity = key
                tables[key][new_entity] = value
                if key == "id" and value.__class__.__name__ == "int":
                    entities.append("p_" + str(value))
                if key == "confseries":
                    if value not in entities:
                        entities.append(value)
            # if element is a list, process each one of its sub-elements accordingly
            elif value.__class__.__name__ == "list":
                if key not in entities:
                    entities.append("p_" + key)
                num_of_subItem = len(value)
                for i in range(num_of_subItem):
                    subvalue = value[i]
                    if subvalue.__class__.__name__ not in ("dict", "list"):
                        if subvalue not in tables[key].keys():
                            new_rel = "has" + key.capitalize() + "_" + str(len(tables[key].keys()))
                            if key == "references":
                                tables[key][new_rel] = "P_" + str(subvalue)
                                if "P_" + str(subvalue) not in entities:
                                    entities.append("P_" + str(subvalue))
                            else:
                                tables[key][new_rel] = subvalue
                            if subvalue not in entities and key in [
                                "topics"]:  # ,"cso_syntactic_topics","cso_semantic_topics","cso_enhanced_topics"
                                entities.append(subvalue)
                            if new_rel not in relations:
                                relations.append(new_rel)
                    if subvalue.__class__.__name__ == "dict":
                        author_id = subvalue.get("id", None)
                        del subvalue['id']
                        list_name_for_sub = "a_" + str(author_id)
                        if list_name_for_sub not in entities:
                            entities.append(str(list_name_for_sub).replace(" ", "_"))
                        tables[key][list_name_for_sub] = dict()
                        for sec_lev_key, sec_lev_val in subvalue.items():  # Author will come to this loop
                            if sec_lev_key not in tables[key].keys():
                                new_rel = "has" + sec_lev_key.capitalize()
                                tables[key][list_name_for_sub][new_rel] = str(sec_lev_val).replace(" ", "_")
                                if new_rel not in relations:
                                    relations.append(new_rel)
                                if sec_lev_key == "name" or sec_lev_key == "country" or sec_lev_key == "affiliation":
                                    if sec_lev_val not in entities:
                                        entities.append(sec_lev_val)
            elif value.__class__.__name__ == "dict":
                for subkey, subvalue in value.items():
                    subvalue = round(subvalue, 2)
                    if subkey not in tables[key].keys():
                        tables[key][subkey] = str(subvalue).replace(" ", "_")
                        entities.append(subkey)

            else:
                raise Exception("Unresolved branch!")

    return tables, entities, relations


def get_entity_tables(load_indicator: bool, in_json_file: str, out_bin_file: str, out_ent_file: str, out_rel_file: str,
                      verbose: bool = False) -> dict:
    all_entities = []
    all_relations = []
    if load_indicator is False:

        if verbose:
            print(termcolor.colored("Scanning...", color="magenta"))

        with open(in_json_file) as file:
            total_lines = str(sum(1 for line in open(in_json_file)))
            for (index, line) in enumerate(file):
                if index > 0 :
                    print("total " + str(index) + " outof " + total_lines)
                    tables = None
                    entities = []
                    relations = []
#hasTopics
                    # send_to_another_file = ("Paper_Id","hasAffiliation","hasTopics", "type", "hasCitationcount", "hasCso_annotated", "hasPapertitle",
                    #                         "hasAbstract", "hasDoi", "hasUrls", "hasOrder", "hasPapertitle", "hasName",
                    #                         "hasGridid", "hasType_dbpedia", "hasCso_syntactic_topics",
                    #                         "hasCso_semantic_topics", "hasCso_enhanced_topics","hasConfplace")
                    send_to_another_file = (
                    "Paper_Id", "hasCitationcount", "hasCso_annotated",
                    "hasPapertitle",
                    "hasAbstract", "hasDoi", "hasUrls", "hasOrder", "hasPapertitle","hasGridid")

                    (tables, entities, relations) = refresh_entity_table(line=line, tables=tables, entities=entities,
                                                                    relations=relations)

                    if len(tables) > 0 and len(entities) and len(relations) > 0:
                        id = str(next(iter(tables["id"].values())))
                        # if id == "2073312981":
                        #     print("Checking for this data set")
                        if verbose:
                            print(termcolor.colored("Finished scanning", color="magenta"))
                        if verbose:
                            print(termcolor.colored("Saving entity and relation files... ", color="magenta"))
                        # Writing for the final triple:
                        triple = ""
                        triple_for_another = ""
                        triple_for_another += str(entities[entities.index("p_" + id + "")]) + "\t" + str(
                            tables["__relation__"]["id"]) + "\t" + str(next(iter(tables["id"].values()))) + "\n"
                        triple += str(
                            entities[entities.index("p_" + id + "")]) + "\t" + "hasEntityType" + "\t" + "Paper" + "\n"
                        if "hasEntityType" not in relations:
                            relations.append("hasEntityType")

                        for key in tables:
                            if key not in ("__relation__", "id") and "p_" + key not in entities and key not in (
                                    "countries", "types"):
                                kye_val_rel = str(tables["__relation__"][key])
                                kye_val = str(next(iter(tables[key].values()))).replace(" ", "_").strip("_")
                                if kye_val == "" or kye_val == "nan":
                                    continue
                                if kye_val_rel == "hasYear":
                                    datestring = kye_val
                                    dt = datetime.strptime(datestring, "%Y-%m-%d")
                                    if int(dt.year) >= 2015:
                                        kye_val = "recent"
                                    if int(dt.year) >= 2010 and int(dt.year) < 2015:
                                        kye_val = "medium_recent"
                                    if int(dt.year) >= 2005 and int(dt.year) < 2010:
                                        kye_val = "medium_old"
                                    if int(dt.year) >= 2000 and int(dt.year) < 2005:
                                        kye_val = "old"
                                    if int(dt.year) < 2000:
                                        kye_val = "very_old"

                                # triple filter on demand
                                if kye_val_rel in send_to_another_file:
                                    triple_for_another += str(entities[entities.index(
                                        "p_" + id + "")]) + "\t" + kye_val_rel + "\t" + kye_val.replace(" ", "_").strip("_") + "\n"
                                else:
                                    triple += str(entities[entities.index(
                                        "p_" + id + "")]) + "\t" + kye_val_rel + "\t" + kye_val.replace(" ", "_").strip("_") + "\n"

                            elif key not in ("__relation__", "id") and "p_" + key not in entities and key in (
                                    "countries", "types"):
                                kye_val_rel = str(tables["__relation__"][key])
                                kye_vals = tables[key]
                                for k in kye_vals:
                                    triple += str(
                                        entities[entities.index("p_" + id + "")]) + "\t" + kye_val_rel + "\t" + str(
                                        k).replace(" ", "_").strip("_") + "\n"
                                    triple_for_another += str(entities[entities.index(
                                        "p_" + id + "")]) + "\t" + "is" + k.capitalize() + "\t" + str(
                                        kye_vals[k]).replace(" ", "_").strip("_") + "\n"

                                    if float(kye_vals[k]) > 50 and len(kye_vals) > 1:
                                        triple += str(entities[entities.index(
                                            "p_" + id + "")]) + "\t" + "hasStrong" + key[
                                                                                     0:-1].capitalize() + "" + "\t" + str(
                                            k).replace(" ", "_") + "\n"
                                        all_relations.append("hasStrong" + key[0:-1].capitalize() + "")

                        for (e_in, e_val) in enumerate(entities):
                            if e_val != "p_authors" and e_val != "p_" + id + "" and e_val.startswith("p_"):
                                sub_val_rel = str(tables["__relation__"][str(e_val[2:])])
                                for sub_val in tables[e_val[2:]]:
                                    if sub_val_rel in send_to_another_file:
                                        triple_for_another += "p_" + id + "" + "\t" + sub_val_rel + "\t" + str(
                                            tables[e_val[2:]][sub_val]).replace(" ", "_").strip("_") + "\n"
                                    else:
                                        triple += "p_" + id + "" + "\t" + sub_val_rel + "\t" + str(
                                            tables[e_val[2:]][sub_val]).replace(" ", "_").strip("_") + "\n"

                            if e_in > 0 and e_val == "p_authors":
                                sub_val_rel = str(tables["__relation__"][str(e_val[2:])])
                                for sub_val in tables[e_val[2:]]:
                                    if sub_val_rel in send_to_another_file:
                                        triple_for_another += "p_" + id + "" + "\t" + sub_val_rel + "\t" + sub_val + "\n"
                                    else:
                                        triple += "p_" + id + "" + "\t" + sub_val_rel + "\t" + sub_val + "\n"

                                    triple_for_another += sub_val + "\t" + "authorType" + "\t" + "Person" + "\n"
                                    for second_sub_val in tables[e_val[2:]][sub_val]:
                                        if second_sub_val in send_to_another_file:
                                            triple_for_another += str(sub_val) + "\t" + second_sub_val + "\t" + str(
                                                tables[e_val[2:]][sub_val][second_sub_val]).replace(" ", "_").strip("_") + "\n"
                                        else:
                                            triple += str(sub_val) + "\t" + second_sub_val + "\t" + str(
                                                tables[e_val[2:]][sub_val][second_sub_val]).replace(" ", "_").replace(
                                                '"', '').strip("_") + "\n"

                        if triple is not None:
                            print(termcolor.colored("Writing triple to file ", color="magenta"), end="")
                        file_ent = open("out/Education papers/SW/data.nt", "a+", encoding="utf-8")
                        file_ent.write(str(triple))
                        file_ent.close()
                        print(termcolor.colored(id + " writing complete ", color="green"), end="")

                        if triple_for_another is not None:
                            print(termcolor.colored("Writing triple to another file  ", color="yellow"), end="")
                        file_ent = open("out/Education papers/SW/data_another_set.nt", "a+", encoding="utf-8")
                        file_ent.write(str(triple_for_another))
                        file_ent.close()
                        print(termcolor.colored(id + " writing complete ", color="green"), end="")
        print(termcolor.colored("Data.nt file created successfully ", color="green"), end="")

        print(termcolor.colored("Process running for entity and relation creation\n", color="red"), end="")
        out_relations_dict = "out/Education papers/relations.dict"
        out_entities_dict = "out/Education papers/entities.dict"

        out_data_nt = "out/Education papers/data.nt"
        # out_data_nt="out/trial/data.nt"
        print(termcolor.colored("Data loading...\n", color="cyan"), end="")
        columns = ["subject", "predicate", "object"]
        all_triples = pd.read_table(out_data_nt, delimiter='\t', names=columns, header=None)
        all_triples = pd.DataFrame(all_triples)
        print(termcolor.colored("Data loading successful\n", color="red"), end="")
        print("Creating entity..")
        unique_entity = all_triples["subject"].astype(str).unique().tolist()
        unique_entity += all_triples["object"].astype(str).unique().tolist()
        unique_entity = list(set(unique_entity))

        entity_file = io.open(out_entities_dict, "w+", encoding="utf-8")
        index = 0
        for idx, elem in enumerate(unique_entity):
            entity_file.write(str(index) + "\t" + str(elem).replace(" ", "_") + "\n")
            index += 1
        entity_file.close()

        print("Creating relation")
        unique_relations = all_triples["predicate"].astype(str).unique().tolist()
        entity_file = io.open(out_relations_dict, "w+", encoding="utf-8")
        index = 0
        for idx, elem in enumerate(unique_relations):
            entity_file.write(str(index) + "\t" + str(elem).replace(" ", "_") + "\n")
            index += 1
        entity_file.close()


def main():
    in_json_file = "in/semantic_web/full_Json_AIDA_SW.json"
    tables = get_entity_tables(load_indicator=False, in_json_file=in_json_file,
                               out_bin_file="out/Education papers/tables.bin",
                               out_ent_file="out/Education papers/SW/entities.dict",
                               out_rel_file="out/Education papers/SW/relations.dict", verbose=True)
    print("For the creation of data entity and relation has been complete...\ closing down....")
    pass


if __name__ == "__main__":
    main()
    pass
else:
    pass
